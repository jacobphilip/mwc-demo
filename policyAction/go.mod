module policyactor

go 1.14

require (
	github.com/Shopify/sarama v1.30.1
	k8s.io/apimachinery v0.23.3
	k8s.io/client-go v0.23.3
)
