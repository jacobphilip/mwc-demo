package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/Shopify/sarama"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

type Threshold struct {
	LowerThreshold bool `json:"lowerThreshold"`
	UpperThreshold bool `json:"upperThreshold"`
}

func main() {

	configk, _ := clientcmd.BuildConfigFromFlags("", "/go/src/policy/config")
	clientset, _ := kubernetes.NewForConfig(configk)

	config := sarama.NewConfig()
	config.ClientID = "go-kafka-consumer"
	config.Consumer.Return.Errors = true
	config.Consumer.Offsets.Initial = sarama.OffsetNewest
	config.Consumer.Offsets.Retention = 5 * time.Minute

	brokers := []string{"kafka.default.svc.cluster.local:9092"}

	master, err := sarama.NewConsumer(brokers, config)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	topics, _ := master.Topics()

	consumer, errors := consume(topics, master)

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	msgCount := 0

	doneCh := make(chan struct{})
	go func() {
		for {
			select {
			case msg := <-consumer:
				msgCount++
				//fmt.Println("Received messages", string(msg.Key), string(msg.Value))
				var result map[string]interface{}
				json.Unmarshal(msg.Value, &result)
				resp, err := http.Post("http://amcop-policy.default.svc.cluster.local:8181/v0/data/tca3", "application/json", bytes.NewBuffer(msg.Value))
				if err != nil {
					log.Println(err)
				}
				body, err := io.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				var thresh Threshold
				json.Unmarshal(body, &thresh)
				client := clientset
				if thresh.UpperThreshold {
					/******************************************/
					fmt.Println("Upper threshold")
					s, err := client.AppsV1().Deployments("default").GetScale(context.TODO(), "v1-amf", metav1.GetOptions{})
					if err != nil {
						log.Println(err)
					}

					sc := *s
					if sc.Spec.Replicas < 2 {
						sc.Spec.Replicas = 2
						fmt.Println("Setting  number of replicas to ", sc.Spec.Replicas)
						_, err := client.AppsV1().Deployments("default").UpdateScale(context.TODO(), "v1-amf", &sc, metav1.UpdateOptions{})
						if err != nil {
							log.Fatal(err)
						}

					}
					/****************************************/
				}
			case consumerError := <-errors:
				msgCount++
				fmt.Println("Received consumerError ", string(consumerError.Topic), string(consumerError.Partition), consumerError.Err)
				//doneCh <- struct{}{}
			case <-signals:
				fmt.Println("Interrupt is detected")
				//doneCh <- struct{}{}
			}
		}
	}()

	<-doneCh
	fmt.Println("Processed", msgCount, "messages")

}

func consume(topics []string, master sarama.Consumer) (chan *sarama.ConsumerMessage, chan *sarama.ConsumerError) {
	consumers := make(chan *sarama.ConsumerMessage)
	errors := make(chan *sarama.ConsumerError)
	for _, topic := range topics {
		if strings.Contains(topic, "__consumer_offsets") {
			continue
		}
		partitions, _ := master.Partitions(topic)
		consumer, err := master.ConsumePartition(topic, partitions[0], sarama.OffsetOldest)
		if nil != err {
			fmt.Printf("Topic %v Partitions: %v", topic, partitions)
			panic(err)
		}
		fmt.Println(" Start consuming topic ", topic)
		go func(topic string, consumer sarama.PartitionConsumer) {
			for {
				select {
				case consumerError := <-consumer.Errors():
					errors <- consumerError
					fmt.Println("consumerError: ", consumerError.Err)

				case msg := <-consumer.Messages():
					consumers <- msg
				}
			}
		}(topic, consumer)
	}

	return consumers, errors
}
